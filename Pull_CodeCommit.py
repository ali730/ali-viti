#!/bin/bash
yum update -y
yum install -y httpd

# Start Apache and enable it on boot
systemctl start httpd
systemctl enable httpd

# Install Git (if not already installed)
yum install -y git

# Configure Git with your name and email (replace with your details)
git config --global user.name "Your Name"
git config --global user.email "your.email@example.com"

# Clone your CodeCommit repository (replace <repository-url> with your repo URL)
git clone https://git-codecommit.<region>.amazonaws.com/v1/repos/<repository-name>

# Replace <repository-name> with your CodeCommit repository name
cd <repository-name>

# Pull the latest code (replace <branch-name> with your desired branch)
git pull origin <branch-name>

# Place your Apache content in the web server's document root
cp -r * /var/www/html/

# Restart Apache to serve the updated content
systemctl restart httpd
