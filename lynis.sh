#!/bin/bash

# User data logging
exec > >(tee /var/log/user-data.log) 2>&1

# Install updates
yum install -y update

# Install EFS Utilities
yum install -y amazon-efs-utils

# Install Lynis
amazon-linux-extras install lynis -y

# Install botocore
yum install python3-pip -y
pip3 install botocore --upgrade

# Create mount directory
mkdir -p /home/home-directories

# Mount EFS TO instance
mount -t efs -o tls fs-0f4bc8af4714c5a46:/ home-directories

# Append /etc/fstab to automate
echo "fs-0f4bc8af4714c5a46:/ home-directories efs _netdev,noresvport,tls,iam 0 0" >> /etc/fstab

# display banner/message after NLB SSH authentication
vi /etc/motd
* * * * * * * W A R N I N G * * * * * * * * * *
This computer system is the property of ProCore Plus. It is for authorized use only. By using this system, all users acknowledge notice of, and agree to comply with, the Acceptable Use of Information Technology Resources Policy ("AUP").   Unauthorized or improper use of this system may result in administrative disciplinary action, civil charges/criminal penalties, and/or other sanctions as set forth in the AUP. By continuing to use this system you indicate your awareness of and consent to these terms and conditions of use. LOG OFF IMMEDIATELY if you do not agree to the conditions stated in this warning.
* * * * * * * * * * * * * * * * * * * * * * * *
update-motd --disable
ssh -o StrictHostKeyChecking=no
service sshd restart

